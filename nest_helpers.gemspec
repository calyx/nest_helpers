$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "nest_helpers/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "nest_helpers"
  spec.version     = NestHelpers::VERSION
  spec.authors     = ["elijah"]
  spec.email       = ["elijah@riseup.net"]
  spec.homepage    = "https://0xacab.org/calyx/nest_helpers"
  spec.summary     = "Various utilities for Ruby on Rails web applications"
  spec.description = "Various utilities for Ruby on Rails web applications"
  spec.license     = "MIT"


  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.3"
  spec.add_dependency "kramdown"

  spec.add_development_dependency "sqlite3"
end
